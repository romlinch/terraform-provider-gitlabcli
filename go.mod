module gitlab.com/romlinch/terraform-provider-gitlabcli

require (
	github.com/avast/retry-go v2.4.3+incompatible
	github.com/hashicorp/go-hclog v0.10.0 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/hashicorp/hcl/v2 v2.1.0 // indirect
	github.com/hashicorp/terraform-config-inspect v0.0.0-20191121111010-e9629612a215 // indirect
	github.com/hashicorp/terraform-plugin-sdk v1.4.0
	github.com/hashicorp/terraform-svchost v0.0.0-20191119180714-d2e4933b9136 // indirect
	github.com/hashicorp/yamux v0.0.0-20190923154419-df201c70410d // indirect
	github.com/xanzy/go-gitlab v0.22.2

)

go 1.13
