package gitlab

import (
	"fmt"
	"github.com/xanzy/go-gitlab"
	"testing"

	"github.com/hashicorp/terraform-plugin-sdk/helper/acctest"
	"github.com/hashicorp/terraform-plugin-sdk/helper/resource"
)

func TestAccGitlabRepositoryFile_drift(t *testing.T) {
	path := acctest.RandStringFromCharSet(10, acctest.CharSetAlphaNum)
	pId := 16178573
	branch := "master"
	content := "mycontent"

	resource.Test(t, resource.TestCase{
		PreCheck:     func() { testAccPreCheck(t) },
		Providers:    testAccProviders,
		Steps: []resource.TestStep{
			// Create a file
			{
				Config: testAccGitlabRepositoryFileConfig(pId, path, branch, content),
				Check: resource.ComposeTestCheckFunc(

				),
			},
			// Nothing to do
			{
				PreConfig: func() {
					conn := testAccProvider.Meta().(*gitlab.Client)
					updateOptions := &gitlab.UpdateFileOptions{
						Branch:        gitlab.String(branch),
						Content:       gitlab.String("modified"),
						CommitMessage: gitlab.String("Updated file via terraform"),
					}
					conn.RepositoryFiles.UpdateFile(pId, path, updateOptions)

				},
				Config: testAccGitlabRepositoryFileConfig(pId, path, branch, content),
				Check: resource.ComposeTestCheckFunc(

				),
			},
		},
	})
}


func testAccGitlabRepositoryFileConfig(pId int, path string, branch string, content string) string {
	return fmt.Sprintf(`
resource "gitlabcli_repository_file" "foo" {
  project_id = %d
  file_path = "%s"
  branch = "%s"
  content = "%s"
}
  `, pId, path, branch, content)
}

