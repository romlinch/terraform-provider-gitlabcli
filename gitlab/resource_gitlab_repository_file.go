package gitlab

import (
	"encoding/base64"
	"fmt"
	"github.com/avast/retry-go"
	"github.com/hashicorp/terraform-plugin-sdk/helper/schema"
	"github.com/xanzy/go-gitlab"
	"log"
)

func resourceGitlabRepositoryFile() *schema.Resource {

	return &schema.Resource{
		Create: resourceGitlabRepositoryFileCreate,
		Read:   resourceGitlabRepositoryFileRead,
		Update: resourceGitlabRepositoryFileUpdate,
		Delete: resourceGitlabRepositoryFileDelete,
		Importer: &schema.ResourceImporter{
			State: schema.ImportStatePassthrough,
		},

		Schema: map[string]*schema.Schema{
			"project_id": {
				Type:     schema.TypeString,
				ForceNew: true,
				Required: true,
			},
			"file_path": {
				Type:     schema.TypeString,
				ForceNew: true,
				Required: true,
			},
			"branch": {
				Type:     schema.TypeString,
				ForceNew: true,
				Required: true,
			},
			"content": {
				Type:     schema.TypeString,
				Required: true,
				ForceNew: false,
			},
		},
	}
}

func resourceGitlabRepositoryFileCreate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*gitlab.Client)

	filePath := d.Get("file_path").(string)
	branch := d.Get("branch").(string)
	projectId := d.Get("project_id").(string)
	content := d.Get("content").(string)
	log.Printf("[DEBUG] Create gitlab repository file %v for %s in ref %s", filePath, projectId, branch)

	cf := &gitlab.CreateFileOptions{
		Branch:        gitlab.String(branch),
		Content:       gitlab.String(content),
		CommitMessage: gitlab.String("Terraform creation"),
	}

	file, _, err := client.RepositoryFiles.CreateFile(projectId, filePath, cf)
	if err != nil {
		log.Printf("[DEBUG] failed to create file %s: %s", filePath, err)
		return err
	}
	d.SetId(buildThreePartID(&projectId, &file.FilePath, &file.Branch))
	return resourceGitlabRepositoryFileRead(d, meta)
}

func resourceGitlabRepositoryFileRead(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*gitlab.Client)
	id := d.Id()

	projectId, filePath, branch, err := projectIdAndfilePathdAndBranchFromId(id)
	log.Printf("[DEBUG] Read gitlab repository file %v for %s in ref %s", filePath, projectId, branch)
	if err != nil {
		return err
	}

	gf := &gitlab.GetFileOptions{
		Ref: gitlab.String(branch),
	}

	err = retry.Do(
		func() error {
			f, _, err := client.RepositoryFiles.GetFile(projectId, filePath, gf)
			if err == nil {
				resourceGitlabRepositoryFileSetToState(d, &projectId, &filePath, &branch, f)
			}
			return err
		})

	if err != nil {
		log.Printf("[DEBUG] failed to read file %s: %s", id, err)
		d.SetId("")
		return err
	}

	return nil
}

func projectIdAndfilePathdAndBranchFromId(id string) (string, string, string, error) {
	projectId, filePath, branch, err := parseThreePartID(id)
	if err != nil {
		return "", "", "", fmt.Errorf("Error parsing ID: %s", id)
	}
	return projectId, filePath, branch, nil
}

func resourceGitlabRepositoryFileUpdate(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*gitlab.Client)

	filePath := d.Get("file_path").(string)
	branch := d.Get("branch").(string)
	projectId := d.Get("project_id").(string)
	content := d.Get("content").(string)

	log.Printf("[DEBUG] Update gitlab repository file %v for %s in ref %s", filePath, projectId, branch)

	updateOptions := &gitlab.UpdateFileOptions{
		Branch:        gitlab.String(branch),
		Content:       gitlab.String(content),
		CommitMessage: gitlab.String("Updated file via terraform"),
	}

	_, _, err := client.RepositoryFiles.UpdateFile(projectId, filePath, updateOptions)
	if err != nil {
		log.Printf("[DEBUG] GitlabRepositoryFileUpdate %q", projectId)
		log.Printf("[DEBUG] GitlabRepositoryFileUpdate %q", filePath)
		log.Printf("[DEBUG] GitlabRepositoryFileUpdate %q", err)
		log.Fatal(err)
		return err
	}

	return resourceGitlabRepositoryFileRead(d, meta)
}

func resourceGitlabRepositoryFileDelete(d *schema.ResourceData, meta interface{}) error {
	client := meta.(*gitlab.Client)

	id := d.Id()
	projectId, filePath, branch, err := projectIdAndfilePathdAndBranchFromId(id)
	if err != nil {
		return err
	}

	log.Printf("[DEBUG] Delete gitlab repository file %v for %s in ref %s", filePath, projectId, branch)

	deleteOperation := &gitlab.DeleteFileOptions{
		Branch:        gitlab.String(branch),
		CommitMessage: gitlab.String("Deleted by terraform"),
	}

	err = retry.Do(
		func() error {
			_, err = client.RepositoryFiles.DeleteFile(projectId, filePath, deleteOperation)
			return err
		})

	return err
}

func resourceGitlabRepositoryFileSetToState(d *schema.ResourceData, projectId *string, filePath *string, branch *string, content *gitlab.File) {
	d.Set("project_id", projectId)
	d.Set("file_path", filePath)
	d.Set("branch", branch)

	data, err := base64.StdEncoding.DecodeString(content.Content)

	if err != nil {

		fmt.Println("error:", err)

		return

	}
	d.Set("content", string(data))
	d.SetId(buildThreePartID(projectId, filePath, branch))
}
