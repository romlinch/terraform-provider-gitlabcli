package main

import (
	"github.com/hashicorp/terraform-plugin-sdk/plugin"
	"gitlab.com/romlinch/terraform-provider-gitlabcli/gitlab"
)

func main() {
	plugin.Serve(&plugin.ServeOpts{
		ProviderFunc: gitlab.Provider})
}